import React from 'react';
import './App.css';
import data from './data.json';
import Header from './components/header.js';
import Content from './components/content';
import Footer from './components/footer';

function App() {
  
  return (
    <div>
      
      <Header />
      <Content data={data}/>
      <Footer />
    </div>
  );
}

export default App;
