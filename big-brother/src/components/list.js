import React, { useState} from 'react';
import ReactDOM from 'react-dom';
import Moreinfo from './moreInfo.js';
import Purchases from './purchases'
import MyMap from './map'

export default function List(props){
    let data = props.data
    let lastIdClicked 
    const [changingClass,setChangingClass]=useState("")

   function changeClass(elmt){
        let purchases = elmt.purchases
        let className =""
        purchases.forEach(element => {
            if(element.amount>=3500){
                className="text-danger"
            }
        });
        return className
   }
   

    function addMoreInfo(event){
        let elmtId= event.target.id
        let div

        if(lastIdClicked){
            div = document.getElementsByClassName(lastIdClicked)[0]

            if(div.hasChildNodes()){
                ReactDOM.unmountComponentAtNode(div)
            }
        }
       

        ReactDOM.render(
            <React.StrictMode>
                <Moreinfo data={data} id={elmtId}/>
            </React.StrictMode>,
            document.getElementsByClassName(elmtId)[0]
        );
        lastIdClicked = elmtId;
        event.preventDefault()
    }
    
    return(
        <section className="container">
            <div className="row d-flex justify-content-around">
            
                {data.map((elmt,index)=>(
                        
                    <div className="col-sm-5 col-11 card mt-4 p-0" key={index}>
                        <div className="container">
                        <h6 className={"text-center "+changeClass(elmt)} id={elmt._id+"a"}>{elmt.name.last} {elmt.name.first}</h6> 
                           <div className="row">
  
                                <div className="row col-12 p-0">
                                    <a href="#" className="col-6"><img src={elmt.picture} className="m-3 border border-dark"alt="super"  id={elmt._id} onClick={addMoreInfo}></img></a>
        
                                    <div className={"col-6 remove pt-3 " +elmt._id}></div>
                                </div>
                                    
           
                                    <div className="col-12 row">
                                        <Purchases elmt={elmt}/> 
                                        <MyMap elmt={elmt}/>
                                    </div>

                           </div>
                        </div>
                            
                    </div>
                ))}

            </div>
        </section>
    )
}