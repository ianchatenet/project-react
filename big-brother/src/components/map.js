import React , {useState} from 'react'

import {Map, TileLayer, Marker} from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster';
import Modal from 'react-bootstrap/Modal'
import { Popup } from 'react-leaflet';




export default function MyMap(props){
    let positions = props.elmt.positions

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function randomDate(start, end) {
        var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())).toLocaleDateString('fr-CA', options);
      }

    const position =positions[0].split(', ')


    return <div>
        <a href="#" className="col" onClick={handleShow}>Déplacements</a>
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Déplacements de {props.elmt.name.last} {props.elmt.name.first}  </Modal.Title>
            </Modal.Header>
            <Modal.Body> 
                        
                <Map center={position} zoom="2" className="myMap">
                    <TileLayer attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                    <MarkerClusterGroup>
                        {positions.map((elmt,index)=>(
                            <Marker position={elmt.split(', ')} key={index}>
                                <Popup>
                                    
                                    <h5>Le {randomDate(new Date(2000,0,1),new Date()).toString().slice(0,24)}</h5>
                                </Popup>
                            </Marker>
                        ))}
                    </MarkerClusterGroup>
                            
                </Map>
            </Modal.Body>
        </Modal>
        
    </div> 
}