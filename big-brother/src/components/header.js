import React from 'react';

function Header(){
    document.body.classList.add('bg-dark');
    return (
        <header className="text-light p-3">
            <h1 className="text-left mt-3">Big Brother</h1>
            <p className="blockquote-footer text-light">We are watching you!</p>
        </header>
    );
}

export default Header;
