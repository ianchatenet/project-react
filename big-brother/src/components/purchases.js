import React , {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import ListGroup from 'react-bootstrap/ListGroup'
import ListGroupItem from 'react-bootstrap/ListGroupItem'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Form } from 'react-bootstrap'

export default function Purchases(props){
    let elmt = props.elmt
    
    let purchases = props.elmt.purchases
    

    let [data,setData]=useState(purchases);
    let [filterDate,setFilterDate]=useState(purchases)

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


   function handleChange(event){
        let id = event.target.id
        let tmp = event.target.value

        if(id==="amount"){
            changeAmount(tmp,event)
        }else{
            changeDate(tmp)
        }  
    }

    function changeAmount(tmp,event){

        if(tmp===""){
            event.target.value=0
            tmp=0
            let amount= parseInt(tmp)
            let filtered = filterDate.filter(elm=>elm.amount>=amount)
            setData(filtered)        
        }else if(isNaN(tmp)){
            event.target.value=0
             tmp =0
             return
         }else{
             let amount = parseInt(tmp)
             let filtered = data.filter(elm=>elm.amount>=amount)
             setData(filtered)
         } 
    }

    function changeDate(tmp){
        if(tmp===""){
            setData(purchases)
            setFilterDate(purchases)
        }else{
            let day = tmp.slice(8,10)
            let month= tmp.slice(5,7)
            let year = tmp.slice(0,4)
            let date = new Date(month+"/"+day+"/"+year)
            let filtered = data.filter(elm=>new Date(elm.date)>=date)
            setData(filtered)
            setFilterDate(filtered)
        }
    }

    function handleClick(){
        setData(purchases)
    }
 
    return  <React.StrictMode>

                <a href="#" className="col" onClick={handleShow}>Achats</a>
                
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Achats de : {elmt.name.last} {elmt.name.first}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body> 
                        <Form.Group>
                            <Form.Control id="amount" size="lg" type="text" placeholder="trier par montant" onChange={handleChange}></Form.Control>
                            <Form.Control id="date"  size="lg" type="date" onChange={handleChange}></Form.Control>
                        </Form.Group>

                        <Button onClick={handleClick}>Reset</Button>

                        <h4>Résultat {data.length}</h4>
                        <ListGroup>
                            {data.map((purchase,index)=>(

                                <ListGroupItem key={index}>
                                    <Row>
                                        <Col>
                                            <p>Montant:{purchase.amount}</p>
                                        </Col>
                                        
                                        <Col>
                                            <p>Date: {purchase.date.slice(3,5)}/{purchase.date.slice(0,2)}/{purchase.date.slice(6,10)} </p>
                                        </Col>
                                    </Row>                                
                                </ListGroupItem>
                            ))}
                        </ListGroup>
                    </Modal.Body>
                </Modal>

            </React.StrictMode>
    
}