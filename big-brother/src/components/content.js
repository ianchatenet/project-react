import React, { useState} from 'react';
import ReactDOM from 'react-dom';
import List from './list.js';

function Content(props){
    let data = props.data;
    let [state,setState]=useState(data);

    function changeEvent(event){
       let tmp=event.target.value;
       let filtered=data.filter(elm=>elm.name.last.includes(tmp))
       let resetAll = document.getElementsByClassName("remove")

       setState(state=filtered)

       for(let i=0;i<resetAll.length;i++){
            if(resetAll[i].hasChildNodes()){
                ReactDOM.unmountComponentAtNode(resetAll[i])
            }
       }
    }
   
        return(
            <React.StrictMode>
                <form className="text-center">
                    <input placeholder="recherche par nom" onChange={changeEvent}/> 
                <p className="text-white">Résultats: {state.length}</p>             
                </form>
                <List data={state}/>
            </React.StrictMode>
        )
}

    

export default Content