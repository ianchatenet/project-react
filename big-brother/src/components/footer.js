import React from 'react';

function Footer(){
    return (
        
        <footer className="app-footer bg-primary text-white mt-5 p-5">
            <h1 className="text-center ml-3">SMALL BROTHER</h1>
            <p className="style text-center">I am also  watching you!</p>
        </footer>
    );
}

export default Footer;